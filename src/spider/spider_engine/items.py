from typing import List
import scrapy


class GoogleResultsItem(scrapy.Item):
    pass


class ResultPage():
    def __init__(self, page_url: str, main_data: str, title_data: str, other_data: str, image_data: List[str], hyper_references: List[str]):
        self.page_url = page_url
        self.main_data = main_data
        self.title_data = title_data
        self.other_data = other_data
        self.image_data = image_data
        self.hyper_references = hyper_references
