import json
import scrapy
from googlesearch import search
from src.spider.utils import scripts, constants
from src.spider.spider_engine.items import ResultPage
import src.analyzer.repository as repository


class GoogleSearchSpider(scrapy.Spider):
    '''
    Scrapy spider worker for extracting and preprocessing website data
    '''
    name = 'google'

    def start_requests(self):
        page_number = constants.RESULT_PAGES_NUMBER_MARGIN * repository.page_number
        searched_phrase = repository.searched_phrase
        
        result_urls = search(searched_phrase, tld="com", lang="en", num=page_number, stop=page_number)
        for ind, url in enumerate(result_urls):
            yield scrapy.Request(url)

    def parse(self, response):
        page_url = response.url

        main_data = ''
        for tag in constants.XPATH_MAIN_TAGS:
            searched_xpath = tag + constants.TAG_CONTENT
            main_data += scripts.encode_content(response.xpath(searched_xpath).getall())

        title_data = ''
        for tag in constants.XPATH_TITLE_TAGS:
            searched_xpath = tag + constants.TAG_CONTENT
            title_data += scripts.encode_content(response.xpath(searched_xpath).getall())

        other_data = ''
        for tag in constants.XPATH_OTHER_TAGS:
            searched_xpath = tag + constants.TAG_CONTENT
            other_data += scripts.encode_content(response.xpath(searched_xpath).getall())

        image_data = response.xpath(constants.IMAGE_TAG).getall()

        hyper_references = response.xpath(constants.HREF_TAG).getall()
        hyper_references = scripts.filter_references(hyper_references)

        yield {
            'page_url': page_url,
            'main_data': main_data,
            'title_data': title_data,
            'other_data': other_data,
            'image_data': image_data,
            'hyper_references': hyper_references
        }

    def close(self):
        pass
        