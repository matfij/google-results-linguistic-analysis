import json
from src.spider.utils import constants, scripts


class JSONPipeline(object):
    '''
    Interceptor class working on spider output data used for saving it to the file specified in /config/constants
    '''

    def open_spider(self, spider):
        scripts.write_data_file('[')

    def process_item(self, item, spider):
        text = json.dumps(item)
        scripts.write_data_file(text + ',', 'a')

    def close_spider(self, spider):
        file_content = scripts.get_data_file_content()
        scripts.write_data_file(file_content[:-1] + ']', 'w')
