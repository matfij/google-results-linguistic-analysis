from typing import List
from unidecode import unidecode
from src.spider.utils import constants
from src.spider.spider_engine.items import ResultPage


def encode_content(results: List[str]) -> str:
    text: str = ' '.join(results)
    filler_tokens = [u"\n", u"\t", u"\r", u"\"", "    "]

    for token in filler_tokens:
        text = text.replace(token, '')
    text = text.replace('  ', ' ')
        
    return unidecode(text)


def filter_references(references: List[str]) -> List[str]:
    valid_references = []
    for reference in references:
        if not reference.find('http'):
            valid_references.append(reference)
    return valid_references


def get_percent(nominator: float, denumerator: float, precision: int = 1) -> str:
    percent = round(100 * (nominator / denumerator), precision)
    return str(percent) + '%'


def write_data_file(data: str, mode = 'w', file_name: str = constants.RESULT_FILE_NAME) -> str:
    with open(file_name, mode) as file:
        file.write(data)


def get_data_file_content(file_name: str = constants.RESULT_FILE_NAME):
    with open(file_name, 'r') as file:
        return file.read()
