'''
File for storing global config constants
'''

LOG_LEVEL = 100

RESULT_PAGES_NUMBER_MARGIN = 2

RESULT_FILE_NAME = 'data.json'

TAG_CONTENT = 'text()'

XPATH_MAIN_TAGS = [
    '//article/',
    '//div/',
    '//span/',
    '//p/',
    '//section/',
    '//main/'
]

XPATH_TITLE_TAGS = [
    '//title/',
    '//header/',
    '//h1/',
    '//h2/',
    '//h3/',
    '//h4/',
    '//h5/',
    '//h6/'
]

XPATH_OTHER_TAGS = [
    '//li/',
    '//td/',
    '//tr/',
    '//abbr/',
    '//strong/',
    '//b/',
    '//bdi/',
    '//bdo/',
    '//blockquote/',
    '//cite/'
]

IMAGE_TAG = '//img/@src'

HREF_TAG = '//a/@href'

CONTENT_LOW_LIMIT = 250
