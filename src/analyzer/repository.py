from typing import List
from src.analyzer.models import ResultPage, SearchResult

'''
File for storing globally required variables
'''

page_number: int = 0

searched_phrase: str = ''

result_pages: List[ResultPage] = []

search_results: List[SearchResult] = []
