from typing import List
import numpy as np
import crochet
crochet.setup()
from scrapy import signals
from scrapy.crawler import CrawlerRunner
from scrapy.signalmanager import dispatcher
from src.spider.spider_engine.spiders.google import GoogleSearchSpider
import src.spider.utils.constants as constants
import src.analyzer.repository as repository
from src.analyzer.models import SearchParams, SearchResult, AnalysisParams, AnalysisResult, ResultPage, AnalysisType
from src.analyzer.services.page_info import PageInfoService
from src.analyzer.services.common_words import CommonWordsService
from src.analyzer.services.references import ReferencesService
from src.analyzer.services.sentences_length import SentencesLengthService


class AnalyzerService:
    '''
    Service-type, proxy class conaining methods for calling scrapping and analyzing modules
    '''
    crawl_runner = CrawlerRunner()
    analyzed_pages: List[ResultPage] = []
    
    def __init__(self):
        self.analysis_switch = {
            AnalysisType.PAGE_INFO.value: self.get_page_info,
            AnalysisType.COMMON_WORDS.value: self.get_common_words,
            AnalysisType.REFERENCES.value: self.get_references,
            AnalysisType.SENTENCES_LENGTH.value: self.get_sentences_length,
            AnalysisType.GRAPHICAL_CONTENT.value: self.get_graphical_content,
            AnalysisType.HYPER_REFERENCES.value: self.get_hyper_references,
        }

    @crochet.run_in_reactor
    def scrape_with_crochet(self, search_params: SearchParams):
        repository.result_pages = []
        repository.search_results = []
        repository.page_number = search_params.page_number
        repository.searched_phrase = search_params.search_phrase
        dispatcher.connect(self.observe_results, signal=signals.item_scraped)
        self.crawl_runner.crawl(GoogleSearchSpider)

    def observe_results(self, item: dict):
        service = PageInfoService()
        word_count = int(service.get_word_count(item))
        if word_count > constants.CONTENT_LOW_LIMIT:
            repository.result_pages.append(dict(item))
            repository.search_results.append({'title': item['title_data']})
            print(len(repository.search_results))

    def analyze(self, analysis_params: AnalysisParams) -> List[AnalysisResult]:
        results: List[AnalysisResult] = []
        self.analyzed_pages = [repository.result_pages[i] for i in analysis_params.analyzed_pages]
        for analysis_type in analysis_params.analysis_types:
            results.append(self.analysis_switch[analysis_type]())
        return results

    def get_page_info(self) -> AnalysisResult:
        service = PageInfoService()
        domains = [service.get_domain(page['page_url']) for page in self.analyzed_pages]
        words = [service.get_word_count(page) for page in self.analyzed_pages]
        domains = '<b>Domains: </b>' + ' '.join(domains)
        words = '<b>Total words: </b>' + ' '.join(words)
        rows = [domains, words]
        return AnalysisResult(AnalysisType.PAGE_INFO.value, result_rows=rows)

    def get_common_words(self) -> AnalysisResult:
        service = CommonWordsService()
        plots = [service.get_common_words(self.analyzed_pages)]
        return AnalysisResult(AnalysisType.COMMON_WORDS.value, images=plots)

    def get_references(self) -> AnalysisResult:
        service = ReferencesService()
        rows = service.get_references(self.analyzed_pages)
        return AnalysisResult(AnalysisType.REFERENCES.value, result_rows=rows)

    def get_sentences_length(self) -> AnalysisResult:
        service = SentencesLengthService()
        plots = [service.get_sentences_lengths(self.analyzed_pages)]
        return AnalysisResult(AnalysisType.SENTENCES_LENGTH.value, images=plots)

    def get_graphical_content(self) -> AnalysisResult:
        service = PageInfoService()
        images = service.get_images(self.analyzed_pages)
        rows = ['<b>Total images: </b>' + str(len(images))]
        return AnalysisResult(AnalysisType.GRAPHICAL_CONTENT.value, images=images, result_rows=rows)

    def get_hyper_references(self) -> AnalysisResult:
        service = PageInfoService()
        rows = service.get_hyper_references(self.analyzed_pages)
        rows.insert(0, '<b>Total hyper-references: ' + str(len(rows)))
        return AnalysisResult(AnalysisType.GRAPHICAL_CONTENT.value, result_rows=rows)
