from typing import List
from enum import Enum

'''
API models definition
'''


class SearchParams:
    def __init__(self, page_number: int, search_phrase: str):
        self.page_number = page_number
        self.search_phrase = search_phrase


class ResultPage:
    def __init__(self, page_url: str, main_data: str, title_data: str, other_data: str, image_data: List[str], hyper_references: List[str]):
        self.page_url = page_url
        self.main_data = main_data
        self.title_data = title_data
        self.other_data = other_data
        self.image_data = image_data
        self.hyper_references = hyper_references


class SearchResult:
    def __init__(self, title: str):
        self.title = title


class AnalysisParams:
    def __init__(self, analyzed_pages: List[int], analysis_types: List[int]):
        self.analyzed_pages = analyzed_pages
        self.analysis_types = analysis_types


class AnalysisType(Enum):
    PAGE_INFO = 0
    COMMON_WORDS = 1
    REFERENCES = 2
    SENTENCES_LENGTH = 3
    GRAPHICAL_CONTENT = 4
    HYPER_REFERENCES = 5


class AnalysisResult:
    def __init__(self, analysis_type: AnalysisType, images: List[str] = [], result_rows: List[str] = []):
        self.analysis_type = analysis_type
        self.images = images
        self.result_rows = result_rows
