import time
import json
from flask import request, Blueprint, views, render_template, jsonify
from src.analyzer.models import SearchParams, ResultPage, AnalysisParams, AnalysisResult
from src.analyzer.analyzer import AnalyzerService
import src.analyzer.repository as repository

'''
API endpoint definition in a form of flask blueprint
'''

def search():
    search_params = SearchParams(**request.get_json())
    service = AnalyzerService()
    service.scrape_with_crochet(search_params)
    while len(repository.search_results) < search_params.page_number:
        pass
    return jsonify(repository.search_results)

def analyze():
    analysis_params = AnalysisParams(**request.get_json())
    service = AnalyzerService()
    results = service.analyze(analysis_params)
    return jsonify([result.__dict__ for result in results])

blueprint = Blueprint('index', __name__)
blueprint.add_url_rule('/search', view_func=search, methods=['POST'])
blueprint.add_url_rule('/analyze', view_func=analyze, methods=['POST'])
