from typing import List
from src.analyzer.models import ResultPage


class PageInfoService:
    '''
    Service-type class conaining methods for extracting base page info data
    '''
    
    def get_domain(self, url: str) -> str:
        protocol_ind = url.index('//') + 2
        if protocol_ind:
            url = url[protocol_ind:]
            post_domain_ind = url.index('/')
            url = url[:post_domain_ind]
            pre_domain_ind = ''.join(url).rindex('.') + 1
            domain = url[pre_domain_ind:]
            return domain
        else:
            return ''

    def get_word_count(self, page: ResultPage) -> str:
        page_content = page['main_data'] + page['other_data']
        word_list = page_content.split()
        return str(len(word_list))

    def get_images(self, pages: List[ResultPage]) -> List[str]:
        imgs = []
        for page in pages:
            imgs.extend(page['image_data'])
        return imgs

    def get_hyper_references(self, pages: List[ResultPage]) -> List[str]:
        hrefs = []
        for page in pages:
            hrefs.extend(page['hyper_references'])
        return hrefs
