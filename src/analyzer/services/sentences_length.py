from typing import List
import base64, io
from collections import Counter
import regex
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from src.analyzer.models import ResultPage


class SentencesLengthService:
    '''
    Service-type class conaining methods for extracting sentences length from page content
    '''
    IMG_PREFIX = 'data:image/png;base64, '

    def get_sentences_lengths(self, pages: List[ResultPage]) -> str:
        content = ''
        for page in pages:
            content += page['main_data'] + page['other_data']

        sentences = regex.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', content)
        sentences_lengths = []
        for sentence in sentences:
            sentence_length = len(regex.findall(r'\s', sentence))
            if sentence_length > 0:
                sentences_lengths.append(sentence_length)

        common_sentences_lengths = Counter(sentences_lengths).most_common(20)

        return self.get_sentences_plot(common_sentences_lengths)

    def get_sentences_plot(self, sentences_lengths: dict) -> str:
        labels, values = zip(*[(sentence[0], sentence[1]) for sentence in sentences_lengths])

        fig, axes = plt.subplots()
        rects = axes.bar(labels, values)

        plt.title('Sentences Lengths')
        plt.xlabel('words')
        plt.ylabel('occurrences')

        img_bytes = io.BytesIO()
        plt.savefig(img_bytes, format='png')
        img_bytes.seek(0)
        
        return self.IMG_PREFIX + str(base64.b64encode(img_bytes.read()))[2:-1]
