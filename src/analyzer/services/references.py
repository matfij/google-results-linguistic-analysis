import regex
from typing import List
from difflib import SequenceMatcher
from src.analyzer.models import ResultPage


class ReferencesService:
    '''
    Service-type class conaining methods for extracting references from page content
    '''
    patterns = [
        r'(?:[\w \.&]+\, )+[0-9]{4}',
        r'[A-Z][a-z]+,\s[A-Z][a-z]?\.[^\)]*,\s\d{4}',
        r'([^"\)]*|"[^"\)]*")(, )([\d]+|n\.d\.|[\d]+[\w])',
        r'([A-Z][a-z|A-Z]+(,|\s).){1,5}([A-Z][a-z|A-Z]+\s)+((\(|\s)\d{4}(\)|\s))\.\s("|[A-Z])(([a-z]|[A-Z]|.)){1,5}\.',
        r'\[[1-9]\d{0,2}\]([A-Z]\..[A-Z]\w+){1,5}."[A-Z]\w*\w+"',
        r'\[[1-9]\d{0,2}\]([A-Z]\..[A-Z]\w+){1,5}.{1,3}"[A-Z]\w*\w+"',
        r'\[[1-9]\d{0,2}\]([A-Z]\..[A-Z]\w+){1,5}.{1,3}[A-Z]\w*\w+',
        r'([A-Z]\..([A-Z]|[a-z])\w+){1,5}.{1,3}([A-Z]|[a-z])\w*\w+\.'
    ]
    MIN_REFERENCE_LENGTH = 50
    MAX_REFERENCE_LENGTH = 250
    MAX_ALLOWED_SIMILARITY = 0.9

    def get_references(self, pages: List[ResultPage]) -> str:
        content = ''
        for page in pages:
            content += page['main_data'] + page['other_data']

        matches = []
        for pattern in self.patterns:
            match = regex.search(pattern, content)
            if match and self.MIN_REFERENCE_LENGTH < len(match[0]) < self.MAX_REFERENCE_LENGTH:
                max_similarity = 0
                for added_match in matches:
                    current_similarity = SequenceMatcher(None, match[0], added_match).ratio()
                    if current_similarity > max_similarity:
                        max_similarity = current_similarity
                if max_similarity < self.MAX_ALLOWED_SIMILARITY:
                    matches.append(match[0])

        matches.insert(0, '<b>Total references: ' + str(len(matches)))
        return matches
