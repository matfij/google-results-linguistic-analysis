from typing import List
import base64, io
from collections import Counter
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from src.analyzer.models import ResultPage


class CommonWordsService:
    '''
    Service-type class conaining methods for extracting most common words in a form of bar plot returned as base64 string
    '''
    IMG_PREFIX = 'data:image/png;base64, '

    def get_common_words(self, pages: List[ResultPage]) -> str:
        content = ''
        for page in pages:
            content += page['main_data'] + page['other_data']

        content = content.split()
        common_words = Counter(content).most_common(10)

        return self.get_word_plot(common_words)

    def get_word_plot(self, common_words: dict) -> str:
        labels, values = zip(*[(word[0], word[1]) for word in common_words])

        fig, axes = plt.subplots()
        rects = axes.bar(labels, values)
        plt.title('Common Words')
        plt.xlabel('words')
        plt.ylabel('occurrences')

        img_bytes = io.BytesIO()
        plt.savefig(img_bytes, format='png')
        img_bytes.seek(0)
        
        return self.IMG_PREFIX + str(base64.b64encode(img_bytes.read()))[2:-1]
