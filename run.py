import os
from flask import Flask, render_template, jsonify, request, redirect, url_for
from src.analyzer import endpoints
from flask_cors import CORS

'''
Run file for creating and starting flask applicatio
'''

def create_app():
    app = Flask(__name__)
    app.register_blueprint(endpoints.blueprint, url_prefix='/')
    CORS(app)

    return app

if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)
