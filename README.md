# Google Results - Linguistic Analysis
Python app for quantitative and qualitative text analysis of google results pages. Scraping is performed incorporating the Scrapy framework. Web application is created with Flask framework.

## Requirements
```
py -m venv .venv
.venv\Scripts\activate
pip install -r requirements.txt
deactivate
```

## Starting app (backend)
```
py run.py
```
